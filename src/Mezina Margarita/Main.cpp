#include "Employee.h"
#include "Cleaner.h"
#include "Driver.h"
#include "Tester.h"
#include "Programmer.h"
#include "TeamLeader.h"
#include "Manager.h"
#include "ProjectManager.h"
#include "SeniorManager.h"
#include <vector>
#include <iostream>
#include <fstream>
using namespace std;
int main() {
	vector<Employee*> employee;
	ifstream fcin("Employeers.txt");
	int id;
	while (fcin >> id) {
		string name, surname, patronymic;
		fcin>>name>>surname>>patronymic;
		string position;
		fcin >> position;
		int worktime;
		fcin >> worktime;
		if (position == "Cleaner") {
			int base;
			fcin >> base;
			employee.push_back(new Cleaner(id, name + " " + surname + " " + patronymic, base, worktime));
		}
		if (position == "Driver") {
			int base;
			fcin >> base;
			employee.push_back(new Driver(id, name + " " + surname + " " + patronymic, base, worktime));
		}
		if (position == "Tester") {
			int base;
			fcin >> base;
			string project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Tester(id, name + " " + surname + " " + patronymic, base, project, contribution, worktime));
		}
		if (position == "Programmer") {
			int base;
			fcin >> base;
			string project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Programmer(id, name + " " + surname + " " + patronymic, base, project, contribution, worktime));
		}
		if (position == "TeamLeader") {
			int base;
			fcin >> base;
			int quality;
			fcin >> quality;
			string project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new TeamLeader(id, name + " " + surname + " " + patronymic, base, project, quality, contribution, worktime));
		}
		if (position == "Manager") {
			string project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Manager(id, name + " " + surname + " " + patronymic, project, contribution, worktime));
		}
		if (position == "ProjectManager") {
			int quality;
			fcin >> quality;
			string project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new ProjectManager(id, name + " " + surname + " " + patronymic, project, quality, contribution, worktime));
		}
		if (position == "SeniorManager") {
			int quality;
			fcin >> quality;
			string project;
			fcin >> project;
			int contribution;
			fcin >> contribution;
			employee.push_back(new SeniorManager(id, name + " " + surname + " " + patronymic, project, quality, contribution, worktime));
		}
	}
	for (int i = 0; i < employee.size(); ++i) {
		employee[i]->SetPayment();
		cout << employee[i]->GetId() << "\t" << employee[i]->GetFio() << "\t" << employee[i]->GetPayment() << "\n";
	}
	return 0;
}